const button = $('.button')
const temp = $('.accordion-title')

$(document).ready(function(){
    $('.hide').hide()
    $('.load').remove()
})

button.on('click', function(){
    let theme = $('link')[0]
    if ($(theme).attr('href') == '/static/home.css') {
        $(theme).prop('href', '/static/home2.css');
        $('.button').text('Go Light');
    } else if ($(theme).attr('href') == '/static/home2.css') {
        $(theme).prop('href', '/static/home.css');
        $('.button').text('Go Dark');
    }
});

temp.on('click', function(){
    let element = $($(this).siblings()).children()[0];
    if ( element.className == 'hide' ) {
        $(element).removeClass('hide').addClass('show');
        $(element).slideDown()
    } else if (element.className == 'show') {
        $(element).removeClass('show').addClass('hide');
        $(element).slideUp()
    }
})
