from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import home_views


# Create your tests here.
class StoryTest(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_ada_url_home_page(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_tidak_ada_url_status_page(self):
        response = self.client.get('/status')
        self.assertEqual(response.status_code, 404)

    def test_method_home_views_terpanggil(self):
        response = resolve(reverse('home:home'))
        self.assertEqual(response.func, home_views)

    def test_ada_tombol_ganti_tema(self):
        response = self.client.get(reverse('home:home'))
        content = response.content.decode('utf8')
        self.assertIn('<p class="button"', content)

