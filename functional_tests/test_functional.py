from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase
from django.urls import reverse
import time

class TestProjectMyStatus(StaticLiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.close()

    def test_ada_title_website(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url)
        # User melihat title website yang dibuka tertulis My Status
        self.assertIn('Home Page', self.browser.title)

    def test_ganti_tema(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url)  
        time.sleep(2)

        #User melihat warna background merupakan warna putih
        backgroundThen = self.browser.find_element_by_class_name('container').value_of_css_property("background-color")

        #User melihat ada tombol untuk mengganti tema dan user meng-klik tombol tersebut
        button = self.browser.find_element_by_class_name('button')
        button.click()
        time.sleep(2)

        #User melihat bahwa setelah meng-klik tombol, warna background berubah
        backgroundNow = self.browser.find_element_by_class_name('container').value_of_css_property("background-color")

        self.assertEqual(backgroundThen, "rgba(245, 245, 245, 1)")
        self.assertEqual(backgroundNow, "rgba(11, 22, 34, 1)")

    def test_bisa_buka_accordion(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url) 
        namaClass = 'class="show"'

        #User melihat bahwa Konten dari accordion tidak muncul
        self.assertNotIn(namaClass, self.browser.page_source)

        elementTitle = self.browser.find_element_by_class_name('accordion-title')
        elementTitle.click()

        #User melihat bahwa Konten dari accordion muncul
        self.assertIn(namaClass, self.browser.page_source)